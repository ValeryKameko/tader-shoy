#pragma once

#include <Windows.h>

namespace winhelper
{

	class WApplication
	{
	public:

		WApplication(HINSTANCE hInstance);

		virtual INT Run();
		
		static void Quit(int exitCode = 0);
		static HINSTANCE GetHInstance();
		
	private:
		static HINSTANCE HInstance;
	};

}