#include "WWindow.h"
#include "WApplication.h"
#include "WClass.h"
#include "WException.h"

namespace winhelper {

	WWindow::WWindow()
	{
	}

	WWindow::~WWindow() 
	{
		if (Hwnd)
			::DestroyWindow(Hwnd);
	}

	void WWindow::Redraw(LPCRECT rect, HRGN region, DWORD flags) const
	{
		::RedrawWindow(Hwnd, rect, region, flags);
	}

	void WWindow::Update() const 
	{
		::UpdateWindow(GetHwnd());
	}

	void WWindow::Show(int nShow) const
	{
		::ShowWindow(Hwnd, nShow);
	}

	void WWindow::InvalidateAll(bool needErase)
	{
		RECT clientRect;
		::InvalidateRect(Hwnd, &clientRect, needErase);
	}

	void WWindow::Reposition(int x, int y, int width, int height)
	{
		MoveWindow(Hwnd, x, y, width, height, true);
	}

	void WWindow::Reposition(const RECT& rect)
	{
		return Reposition(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
	}

	HMENU WWindow::GetMenu() const
	{
		return Menu;
	}

	HWND WWindow::GetHwnd() const
	{
		return Hwnd;
	}

	RECT WWindow::GetClientRect() const
	{
		RECT clientRect;
		::GetClientRect(Hwnd, &clientRect);
		return clientRect;
	}

	HDC WWindow::GetDeviceContext() const
	{
		return ::GetDC(Hwnd);
	}

	WWindow* WWindow::GetParent() const
	{
		return Parent;
	}

	LRESULT WWindow::SendMessageW(UINT uMsg, WPARAM wParam, LPARAM lParam) const 
	{
		return ::SendMessageW(Hwnd, uMsg, wParam, lParam);
	}

	void WWindow::ReleaseDeviceContext(HDC deviceContext) const
	{
		::ReleaseDC(Hwnd, deviceContext);
	}

	void WWindow::InitializeWindow(
		WWindow* parent,
		const WClass& windowClass,
		LPCWSTR lpWindowName,
		DWORD dwStyle,
		DWORD dwExStyle,
		HMENU hMenu,
		int X,
		int Y,
		int nWidth,
		int nHeight)
	{
		Hwnd = CreateWindowEx(
			dwExStyle,
			windowClass.GetName(),
			lpWindowName,
			dwStyle,
			X, Y,
			nWidth, nHeight,
			parent ? parent->GetHwnd() : nullptr,
			hMenu,
			WApplication::GetHInstance(),
			this);
		if (!Hwnd)
			throw WException(GetLastError());
		Parent = parent;
		Menu = hMenu;
	}

	LRESULT WWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		return DefWindowProc(Hwnd, uMsg, wParam, lParam);
	}

	LRESULT WWindow::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		WWindow* window{ nullptr };
		const auto windowUserData = GetWindowLongPtr(hwnd, GWLP_USERDATA);
		window = reinterpret_cast<WWindow*>(windowUserData);
		if (!window && (uMsg == WM_CREATE || uMsg == WM_NCCREATE))
		{
			const auto createStruct = LPCREATESTRUCT(lParam);
			window = static_cast<WWindow*>(createStruct->lpCreateParams);
			SetWindowLongPtr(hwnd, GWLP_USERDATA, LONG_PTR(window));

			window->Hwnd = hwnd;
		}
		if (window)
			return window->HandleMessage(uMsg, wParam, lParam);
		return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	WWindow* WWindow::CreateSystemWindow(
		WWindow* parent,
		WClass windowClass,
		LPCWSTR lpWindowName,
		DWORD dwStyle,
		DWORD dwExStyle,
		HMENU hMenu,
		int X, int Y,
		int nWidth, int nHeight)
	{
		HWND hwnd = CreateWindowEx(
			dwExStyle,
			windowClass.GetName(),
			lpWindowName,
			dwStyle,
			X, Y,
			nWidth, nHeight,
			parent ? parent->GetHwnd() : nullptr,
			hMenu,
			WApplication::GetHInstance(),
			nullptr);
		if (!hwnd)
			throw WException(GetLastError());
		return new WWindow{ hwnd, parent, hMenu };
	}

	WWindow::WWindow(HWND hwnd, WWindow* parent, HMENU menu)
	{
		this->Hwnd = hwnd;
		this->Parent = parent;
		this->Menu = menu;
	}
}
