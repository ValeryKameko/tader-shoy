#include "WApplication.h"

namespace winhelper {

	HINSTANCE WApplication::HInstance = nullptr;
	
	WApplication::WApplication(HINSTANCE hInstance)
	{
		HInstance = hInstance;
	}

	INT WApplication::Run()
	{
		MSG msg{};
		BOOL result;
		while ((result = GetMessage(&msg, nullptr, 0, 0))) {
			if (result == -1)
				return -1;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		return msg.wParam;
	}

	void WApplication::Quit(int exitCode)
	{
		PostQuitMessage(0);
	}

	HINSTANCE WApplication::GetHInstance()
	{
		return HInstance;
	}
}
