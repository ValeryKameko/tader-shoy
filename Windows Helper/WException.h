#pragma once

#include <exception>
#include <Windows.h>
#include <string>

namespace winhelper {
	
	class WException : public std::exception
	{
	public:
		explicit WException(DWORD errorCode);

		char const* what() const override;
		
	private:
		std::string Message;
	};
}

