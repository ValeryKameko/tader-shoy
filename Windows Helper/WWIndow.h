#pragma once

#include <Windows.h>

namespace winhelper {

	class WClass;

	class WWindow
	{
	public:
		WWindow();
		virtual ~WWindow();

		void Redraw(LPCRECT rect, HRGN region, DWORD flags) const;
		void Update() const;
		void Show(int nShow) const;
		void InvalidateAll(bool needErase);
		void Reposition(int x, int y, int width, int height);
		void Reposition(const RECT& rect);

		HMENU GetMenu() const;
		inline HWND GetHwnd() const;
		RECT GetClientRect() const;
		HDC GetDeviceContext() const;
		WWindow* GetParent() const;

		LRESULT SendMessageW(UINT uMsg, WPARAM wParam, LPARAM lParam) const;

		void ReleaseDeviceContext(HDC deviceContext) const;

		static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
		static WWindow* CreateSystemWindow(
			WWindow* parent,
			WClass windowClass,
			LPCWSTR lpWindowName,
			DWORD dwStyle = WS_CHILD,
			DWORD dwExStyle = 0,
			HMENU hMenu = nullptr,
			int X = CW_USEDEFAULT,
			int Y = CW_USEDEFAULT,
			int nWidth = CW_USEDEFAULT,
			int nHeight = CW_USEDEFAULT);

	protected:
		WWindow(HWND hwnd, WWindow* parent = nullptr, HMENU menu = nullptr);

		void InitializeWindow(
			WWindow* parent,
			const WClass& windowClass,
			LPCWSTR lpWindowName,
			DWORD dwStyle = WS_CHILD,
			DWORD dwExStyle = 0,
			HMENU hMenu = nullptr,
			int X = CW_USEDEFAULT,
			int Y = CW_USEDEFAULT,
			int nWidth = CW_USEDEFAULT,
			int nHeight = CW_USEDEFAULT);

		virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	private:
		WWindow* Parent;
		HWND Hwnd;
		HMENU Menu;
	};


}
