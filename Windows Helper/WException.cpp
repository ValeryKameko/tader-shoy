#include "WException.h"
#include <memory>

namespace winhelper {
	WException::WException(DWORD errorCode)
	{
		if (errorCode == 0)
			Message = "Success";
		else {
			LPSTR messageBuffer = nullptr;
			FormatMessageA(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				nullptr, 
				errorCode, 
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				LPSTR(&messageBuffer),
				0, 
				nullptr);

			Message = std::string(messageBuffer);
			
			LocalFree(messageBuffer);
		}
	}

	char const* WException::what() const
	{
		return Message.c_str();
	}

}
