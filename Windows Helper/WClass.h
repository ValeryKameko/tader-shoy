#pragma once

#include <Windows.h>

namespace winhelper {
	class WClass
	{
	public:
		WClass(
			LPCWSTR lpszClassName,
			UINT style,
			HCURSOR hCursor = nullptr,
			HBRUSH hbrBackground = nullptr,
			LPCWSTR lpszMenuName = nullptr);

		LPCTSTR GetName() const;

		static WClass GetSystemWindowClass(LPCWSTR systemClassName);
		static WClass GetDefaultWindowClass();
		static WClass GetDefaultComponentClass();

	private:
		explicit WClass(LPCWSTR lpszSystemClassName);
		
		LPCTSTR Name;
		ATOM Atom;
	};
}
