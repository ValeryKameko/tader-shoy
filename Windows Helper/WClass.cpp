#include "WClass.h"
#include "WWindow.h"
#include "WApplication.h"
#include "WException.h"

namespace winhelper
{
	WClass::WClass(
		LPCWSTR lpszClassName,
		UINT style,
		HCURSOR hCursor,
		HBRUSH hbrBackground,
		LPCWSTR lpszMenuName)
	{
		WNDCLASSEX wndEx{};

		wndEx.cbSize = sizeof(wndEx);
		wndEx.style = style;
		wndEx.lpfnWndProc = WWindow::WindowProc;
		wndEx.cbClsExtra = 0;
		wndEx.cbWndExtra = 0;
		wndEx.hInstance = WApplication::GetHInstance();
		wndEx.hIcon = nullptr;
		wndEx.hCursor = hCursor;
		wndEx.hbrBackground = hbrBackground;
		wndEx.lpszMenuName = lpszMenuName;
		wndEx.lpszClassName = lpszClassName;
		wndEx.hIconSm = nullptr;

		Name = lpszClassName;
		Atom = RegisterClassEx(&wndEx);
		if (!Atom)
			throw WException(GetLastError());
	}
	
	WClass::WClass(LPCWSTR lpszSystemClassName)
	{
		Name = lpszSystemClassName;
		Atom = 0;
	}

	LPCTSTR WClass::GetName() const
	{
		return Name;
	}

	WClass WClass::GetSystemWindowClass(LPCWSTR systemClassName)
	{
		WClass wclass{ systemClassName };
		return wclass;
	}

	WClass WClass::GetDefaultWindowClass()
	{
		static WClass defaultClass{
			L"Default_Window_Class",
			CS_OWNDC | CS_HREDRAW | CS_VREDRAW
		};
		return defaultClass;
	}

	WClass WClass::GetDefaultComponentClass()
	{
		static WClass defaultClass{
			L"Default_Component_Class",
			CS_PARENTDC | CS_HREDRAW | CS_VREDRAW
		};
		return defaultClass;
	}
}
