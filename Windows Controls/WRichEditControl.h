#pragma once

#include "WWindow.h"
#include <string>
#include "RichEdit.h"

namespace wincontrol {

	using namespace winhelper;
	
	class WRichEditControl : public WWindow
	{
	public:
		explicit WRichEditControl(DWORD style, DWORD exStyle = 0, WWindow* parent = nullptr, HMENU menu = nullptr);

		~WRichEditControl();

		std::wstring GetTextW() const;
		std::string GetTextA() const;
	};
}

