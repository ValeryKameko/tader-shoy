#include "WRichEditControl.h"
#include "commctrl.h"
#include "Richedit.h"
#include "WClass.h"
#include <string>
#include <Windows.h>


namespace wincontrol {

	const LPCWSTR RICH_EDIT_LIBRARY = L"Msftedit.dll";
	
	WRichEditControl::WRichEditControl(DWORD style, DWORD exStyle, WWindow* parent, HMENU menu)
	{
		LoadLibrary(RICH_EDIT_LIBRARY);

		InitializeWindow(
			parent,
			WClass::GetSystemWindowClass(MSFTEDIT_CLASS),
			L"",
			style | WS_CHILD,
			exStyle,
			menu);
	}

	WRichEditControl::~WRichEditControl()
	{
		
	}

	std::wstring WRichEditControl::GetTextW() const
	{
		int textLength = GetWindowTextLengthW(GetHwnd());
		std::wstring buffer(textLength, 0);
		::GetWindowTextW(GetHwnd(), buffer.data(), textLength + 1);
		
		return buffer;
	}

	std::string WRichEditControl::GetTextA() const
	{
		int textLength = GetWindowTextLengthA(GetHwnd());
		std::string buffer(textLength, 0);
		::GetWindowTextA(GetHwnd(), buffer.data(), textLength + 1);

		return buffer;
	}
}
