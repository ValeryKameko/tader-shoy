#pragma once
#include "WWindow.h"
#include <memory>
#include <vector>
#include <optional>

namespace wincontrol {

	using namespace winhelper;

	class WTabControl final : public WWindow
	{
	public:
		explicit WTabControl(DWORD style, DWORD exStyle = 0, WWindow* parent = nullptr, HMENU menu = nullptr);

		int GetSelection();

		void InsertTab(int index, LPCWSTR tabName, const std::shared_ptr<WWindow>& tab);
		void AddTab(LPCWSTR tabName, const std::shared_ptr<WWindow>& tab);

		void DeleteTab(const std::shared_ptr<WWindow>& tab);
		void DeleteTab(int index);

		void Switch(int index);

		void Clear();
		
	protected:
		LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) override;
		
	private:
		LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam);
		
		void ResizeTab(const std::shared_ptr<WWindow>& tab);
		void PerformSwitch(int index);


		std::shared_ptr<WWindow> TabWindow;
		std::vector<std::shared_ptr<WWindow>> Tabs;
		std::optional<std::shared_ptr<WWindow>> CurrentTab;
	};
}

