#include "WButtonControl.h"
#include "WClass.h"
#include <CommCtrl.h>
#include "ComCtlInitializer.h"

namespace wincontrol {

	WButtonControl::WButtonControl(LPCWSTR label, DWORD style, DWORD exStyle, WWindow* parent, HMENU menu)
	{
		ComCtlInitializer::GetInstance().Initialize(ICC_WIN95_CLASSES);

		WWindow::InitializeWindow(
			parent,
			WClass::GetSystemWindowClass(WC_BUTTON),
			label,
			style | WS_CHILD,
			exStyle,
			menu);
	}
}
