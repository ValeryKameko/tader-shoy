#include "ComCtlInitializer.h"
#include <CommCtrl.h>
#include "WException.h"

namespace wincontrol {
	
	ComCtlInitializer& ComCtlInitializer::GetInstance()
	{
		static ComCtlInitializer instance;
		return instance;
	}

	void ComCtlInitializer::Initialize(DWORD iccMask)
	{
		INITCOMMONCONTROLSEX iccEx{ sizeof(iccEx), iccMask };
		if (!InitCommonControlsEx(&iccEx))
			throw winhelper::WException(GetLastError());
	}

	ComCtlInitializer::ComCtlInitializer()
	{
	}
}
