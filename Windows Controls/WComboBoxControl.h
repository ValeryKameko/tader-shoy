#pragma once

#include <WWindow.h>

namespace wincontrol {

	using namespace winhelper;

	class WComboBoxControl final : public WWindow
	{
	public:
		explicit WComboBoxControl(DWORD style, DWORD exStyle = 0, WWindow* parent = nullptr, HMENU menu = nullptr);

		void Add(LPCWSTR string);

		void Select(int index);

		int GetSelection();
	};
}
