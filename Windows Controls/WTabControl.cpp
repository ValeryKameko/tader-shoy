#include "WTabControl.h"
#include "WClass.h"
#include <commctrl.h>
#include "ComCtlInitializer.h"
#include <algorithm>

namespace wincontrol {

	const HMENU TAB_WINDOW_MENU = HMENU(10);

	WTabControl::WTabControl(DWORD style, DWORD exStyle, WWindow* parent, HMENU menu)
	{
		ComCtlInitializer::GetInstance().Initialize(ICC_TAB_CLASSES);

		InitializeWindow(
			parent,
			WClass(L"Tab_Control", 0),
			L"",
			WS_CHILD | WS_VISIBLE,
			0,
			menu);

		TabWindow.reset(CreateSystemWindow(
			this,
			WClass::GetSystemWindowClass(WC_TABCONTROL),
			L"",
			style | WS_CHILD | WS_VISIBLE,
			exStyle,
			nullptr,
			0, 0,
			100, 100));
	}

	void WTabControl::InsertTab(int index, LPCWSTR tabName, const std::shared_ptr<WWindow>& tab)
	{
		TCITEM tcItem{};
		tcItem.mask = TCIF_TEXT;
		tcItem.pszText = _wcsdup(tabName);

		Tabs.insert(Tabs.begin() + index, tab);
		TabCtrl_InsertItem(TabWindow->GetHwnd(), index, &tcItem);

		tab->Show(SW_HIDE);
		ResizeTab(tab);

		if (Tabs.size() == 1)
			Switch(0);
	}

	int WTabControl::GetSelection() 
	{
		return std::find(Tabs.begin(), Tabs.end(), CurrentTab) - Tabs.begin();
	}

	void WTabControl::AddTab(LPCWSTR tabName, const std::shared_ptr<WWindow>& tab)
	{
		InsertTab(Tabs.size(), tabName, tab);
	}

	void WTabControl::DeleteTab(const std::shared_ptr<WWindow>& tab)
	{
		int index = find(Tabs.cbegin(), Tabs.cend(), tab) - Tabs.cbegin();
		if (CurrentTab && *CurrentTab == tab)
		{
			if (index + 1 < Tabs.size())
				Switch(index + 1);
			else
				Switch(index - 1);
		}
		DeleteTab(index);
	}

	void WTabControl::DeleteTab(int index)
	{
		Tabs.erase(Tabs.begin() + index);
		TabCtrl_DeleteItem(TabWindow->GetHwnd(), index);
	}

	LRESULT WTabControl::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = LPNMHDR(lParam);
			switch (lpnmhdr->code)
			{
			case TCN_SELCHANGE:
			{
				int selectedIndex = TabCtrl_GetCurSel(TabWindow->GetHwnd());
				PerformSwitch(selectedIndex);
				return 0;
			}
			break;
			default:
				return WWindow::HandleMessage(uMsg, wParam, lParam);
			}
		}
		break;
		case WM_SIZE:
			return OnSize(uMsg, wParam, lParam);
		default:
			return WWindow::HandleMessage(uMsg, wParam, lParam);
		}
	}

	LRESULT WTabControl::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		int width = LOWORD(lParam);
		int height = HIWORD(lParam);

		if (TabWindow)
			TabWindow->Reposition(
				0, 0,
				width, height);
		
		if (CurrentTab)
			ResizeTab(*CurrentTab);
		return 0;
	}

	void WTabControl::ResizeTab(const std::shared_ptr<WWindow>& tab)
	{
		RECT tabRect = TabWindow->GetClientRect();

		TabCtrl_AdjustRect(TabWindow->GetHwnd(), false, &tabRect);

		tab->Reposition(tabRect);
	}

	void WTabControl::PerformSwitch(int index)
	{
		if (CurrentTab)
			(*CurrentTab)->Show(SW_HIDE);

		CurrentTab.reset();

		if (index >= 0 && index < Tabs.size()) {
			CurrentTab = Tabs[index];
			(*CurrentTab)->Show(SW_SHOW);

			ResizeTab(*CurrentTab);
		}
	}

	void WTabControl::Switch(int index)
	{
		PerformSwitch(index);
		if (index >= 0 && index < Tabs.size()) {
			TabCtrl_SetCurSel(TabWindow->GetHwnd(), index);
		}
	}

	void WTabControl::Clear()
	{
		TabCtrl_DeleteAllItems(TabWindow->GetHwnd());
		CurrentTab.reset();
		Tabs.clear();
	}
}
