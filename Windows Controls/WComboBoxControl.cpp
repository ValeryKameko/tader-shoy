#include "WComboBoxControl.h"
#include "ComCtlInitializer.h"
#include <CommCtrl.h>
#include <windowsx.h>
#include <WClass.h>

namespace wincontrol {

	WComboBoxControl::WComboBoxControl(DWORD style, DWORD exStyle, WWindow* parent, HMENU menu)
	{
		ComCtlInitializer::GetInstance().Initialize(ICC_WIN95_CLASSES);

		WWindow::InitializeWindow(
			parent,
			WClass::GetSystemWindowClass(WC_COMBOBOX),
			L"",
			style | WS_CHILD,
			exStyle,
			menu);
	}

	void WComboBoxControl::Add(LPCWSTR string) 
	{
		ComboBox_AddString(GetHwnd(), string);
	}

	void WComboBoxControl::Select(int index) 
	{
		ComboBox_SetCurSel(GetHwnd(), index);
	}

	int WComboBoxControl::GetSelection() 
	{
		return ComboBox_GetCurSel(GetHwnd());
	}
}
