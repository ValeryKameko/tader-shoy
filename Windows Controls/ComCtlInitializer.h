#pragma once
#include <Windows.h>

namespace wincontrol {

	class ComCtlInitializer final
	{
	public:
		static ComCtlInitializer& GetInstance();

		void Initialize(DWORD iccMask);
		
	private:
		ComCtlInitializer();
	};
}

