#pragma once
#include "WWindow.h"
#include <Windows.h>

namespace wincontrol {

	using namespace winhelper;

	class WButtonControl final : public WWindow
	{
	public:
		explicit WButtonControl(LPCWSTR label, DWORD style, DWORD exStyle = 0, WWindow* parent = nullptr, HMENU menu = nullptr);
	};
}

