#include "GlCompilationException.h"

namespace glhelper {

	GlCompilationException::GlCompilationException(const char* message)
		: GlException(message)
	{
	}

	GlCompilationException::GlCompilationException(const std::string& message)
		: GlException(message)
	{
	}


	GlShaderCompilationException::GlShaderCompilationException(const char* message)
		: GlCompilationException(message)
	{
	}

	GlShaderCompilationException::GlShaderCompilationException(const std::string& message)
		: GlCompilationException(message)
	{
	}


	GlProgramBundlingException::GlProgramBundlingException(const char* message)
		: GlCompilationException(message)
	{
	}

	GlProgramBundlingException::GlProgramBundlingException(const std::string& message)
		: GlCompilationException(message)
	{
	}

}
