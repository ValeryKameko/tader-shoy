﻿#include <Windows.h>
#include <GL\glew.h>
#include <GL\wglew.h>

#include "GlException.h"
#include "GlContext.h"
#include "FakeGlContext.h"
#include <iostream>
#include <sstream>

namespace glhelper
{
	const std::vector<int> GlContext::REQUIRED_PIXEL_FORMAT_ATTRIBUTES = {
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_ALPHA_BITS_ARB, 8,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
		WGL_SAMPLES_ARB, 4,
		0 };

	const std::map<int, LPCSTR> GlContext::DEBUG_SOURCE_TO_STRING{
		{GL_DEBUG_SOURCE_API, "API"},
		{GL_DEBUG_SOURCE_WINDOW_SYSTEM, "Window System"},
		{GL_DEBUG_SOURCE_SHADER_COMPILER, "Shader Compiler"},
		{GL_DEBUG_SOURCE_THIRD_PARTY, "Third Party"},
		{GL_DEBUG_SOURCE_APPLICATION, "Application"},
		{GL_DEBUG_SOURCE_OTHER, "Other"}
	};

	const std::map<int, LPCSTR> GlContext::DEBUG_TYPE_TO_STRING{
		{GL_DEBUG_TYPE_ERROR, "Error"},
		{GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, "Deprecated Behaviour"},
		{GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, "Undefined Behaviour"},
		{GL_DEBUG_TYPE_PORTABILITY, "Portability"},
		{GL_DEBUG_TYPE_PERFORMANCE, "Performance"},
		{GL_DEBUG_TYPE_MARKER, "Marker"},
		{GL_DEBUG_TYPE_PUSH_GROUP, "Push Group"},
		{GL_DEBUG_TYPE_POP_GROUP, "Pop Group"},
		{GL_DEBUG_TYPE_OTHER, "Other"},
	};

	const std::map<int, LPCSTR> GlContext::DEBUG_SEVERITY_TO_STRING{
		{GL_DEBUG_SEVERITY_HIGH, "high"},
		{GL_DEBUG_SEVERITY_MEDIUM, "medium"},
		{GL_DEBUG_SEVERITY_LOW, "low"},
		{GL_DEBUG_SEVERITY_NOTIFICATION, "notification"},
	};

	std::vector<int> GlContext::CreateRenderingContextAttributes(
		int majorVersion,
		int minorVersion)
	{
		return {
			WGL_CONTEXT_MAJOR_VERSION_ARB, majorVersion,
			WGL_CONTEXT_MINOR_VERSION_ARB, minorVersion,
			WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0 };
	}

	GlContext::GlContext(HDC deviceContext, int majorVersion, int minorVersion)
	{
		DeviceContext = deviceContext;
		FakeGlContext& fakeContext = FakeGlContext::Instance();
		fakeContext.MakeCurrent();
		
		SetupGlew();
		SetupPixelFormatDescriptor(deviceContext);

		auto contextAttributes = CreateRenderingContextAttributes(majorVersion, minorVersion);
		SetupRenderContext(deviceContext, contextAttributes);

		fakeContext.FreeCurrent();
		MakeCurrent();

		SetupDebugHandler();
	}

	GlContext::~GlContext()
	{
		FreeCurrent();
		wglDeleteContext(RenderContext);
	}

	void GlContext::MakeCurrent()
	{
		if (!wglMakeCurrent(DeviceContext, RenderContext))
			throw GlException("Cannot make current Rendering Context");
	}

	void GlContext::FreeCurrent()
	{
		if (!wglMakeCurrent(DeviceContext, nullptr))
			throw GlException("Cannot free current Rendering Context");
	}

	void GlContext::SetupGlew()
	{
		static bool glewInitialized = false;
		if (!glewInitialized)
		{
			GLenum returnCode = glewInit();
			if (returnCode != GLEW_OK)
				throw GlException("GLEW is not initialized");
			glewInitialized = true;
		}
	}

	void GlContext::SetupDebugHandler()
	{
		GLint contextProfile;
		glGetIntegerv(GL_CONTEXT_FLAGS, &contextProfile);
		if (contextProfile & GL_CONTEXT_FLAG_DEBUG_BIT)
		{
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(GLDEBUGPROC(DebugHandler), nullptr);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
		}
	}

	void GlContext::SetupPixelFormatDescriptor(HDC deviceContext)
	{
		int index;
		auto pfd = FindClosestPixelFormatDescriptor(deviceContext, REQUIRED_PIXEL_FORMAT_ATTRIBUTES, index);
		if (!pfd.get())
			throw GlException("Cannot find closest Pixel Format Descriptor");

		BOOL result = SetPixelFormat(deviceContext, index, &*pfd);
		if (!result)
			throw GlException("Cannot setup Pixel Format Descriptor");
	}

	void GlContext::SetupRenderContext(
		HDC deviceContext,
		const std::vector<int>& contextAttributes)
	{
		if (!wglewIsSupported("WGL_ARB_create_context"))
			throw GlException("WGL_ARB_create_context is not supported");
		RenderContext = wglCreateContextAttribsARB(deviceContext, 0, contextAttributes.data());
		if (!RenderContext)
			throw GlException("Cannot create OpenGL rendering context");
	}

	std::unique_ptr<PIXELFORMATDESCRIPTOR> GlContext::FindClosestPixelFormatDescriptor(
		HDC deviceContext,
		const std::vector<int>& pfdAttributes,
		int& index)
	{
		unsigned int numFormats;
		auto foundPfd = std::make_unique<PIXELFORMATDESCRIPTOR>();

		if (!wglewIsSupported("WGL_ARB_pixel_format"))
			throw GlException("WGL_ARB_pixel_format is not supported");
		BOOL boolResult = wglChoosePixelFormatARB(
			deviceContext,
			pfdAttributes.data(),
			nullptr,
			1,
			&index,
			&numFormats);
		if (!boolResult)
			return nullptr;

		int intResult = DescribePixelFormat(
			deviceContext,
			index,
			sizeof(PIXELFORMATDESCRIPTOR),
			foundPfd.get());
		if (!intResult)
			return nullptr;

		return foundPfd;
	}

	void GlContext::DebugHandler(
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		void* userParam)
	{
		std::ostringstream ss;
		ss << "Source: ";
		if (DEBUG_SOURCE_TO_STRING.count(source))
			ss << "---\n";
		else
			ss << DEBUG_SOURCE_TO_STRING.at(source);
		ss << std::endl;

		ss << "Type: ";
		if (DEBUG_TYPE_TO_STRING.count(type))
			ss << "---\n";
		else
			ss << DEBUG_TYPE_TO_STRING.at(type);
		ss << std::endl;

		ss << "Severity: ";
		if (DEBUG_SEVERITY_TO_STRING.count(severity))
			ss << "---\n";
		else
			ss << DEBUG_SEVERITY_TO_STRING.at(severity);
		ss << std::endl;

		ss << "Message: " << message << std::endl;
	}

} // namespace glhelper