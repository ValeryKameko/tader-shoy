#include <windows.h>
#include <GL/glew.h>
#include <GL/GL.h>

#include "GlShaderProgram.h"
#include "GlShader.h"
#include "GlException.h"
#include "GlCompilationException.h"


namespace glhelper {
	
	GlShaderProgram::GlShaderProgram(const std::string& name)
		: Name(name)
	{
		Id = glCreateProgram();
	}

	GlShaderProgram::~GlShaderProgram()
	{
		glDeleteProgram(Id);
	}

	void GlShaderProgram::AddShader(const std::shared_ptr<GlShader>& shader)
	{
		Shaders.push_back(shader);

		glAttachShader(Id, shader->GetId());
	}

	bool GlShaderProgram::IsValid()
	{
		GLint isValid = 0;
		glGetProgramiv(Id, GL_VALIDATE_STATUS, &isValid);
		return isValid != GL_FALSE;
	}

	void GlShaderProgram::Compile()
	{
		glLinkProgram(Id);
		if (!IsLinked()) {
			std::string linkageLog = GetProgramLog();
			throw GlProgramBundlingException(std::string("Program ") + Name + " is not linked:\n" + linkageLog);
		}

		glValidateProgram(Id);
		if (!IsValid())
		{
			std::string validationLog = GetProgramLog();
			throw GlProgramBundlingException(std::string("Program ") + Name + " is not valid:\n" + validationLog);
		}
	}

	GLuint GlShaderProgram::GetId()
	{
		return Id;
	}


	void GlShaderProgram::Use()
	{
		glUseProgram(Id);
	}

	bool GlShaderProgram::IsLinked()
	{
		GLint isLinked = 0;
		glGetProgramiv(Id, GL_LINK_STATUS, &isLinked);
		return isLinked != GL_FALSE;
	}

	std::string GlShaderProgram::GetProgramLog()
	{
		GLint maxLogLength = 0;
		glGetProgramiv(Id, GL_INFO_LOG_LENGTH, &maxLogLength);

		GLsizei logLength;
		std::vector<GLchar> infoLog(maxLogLength);

		glGetProgramInfoLog(Id, maxLogLength, &logLength, infoLog.data());

		std::string logString;
		for (GLchar ch : infoLog)
			logString += char(ch);
		
		return logString;
	}
}
