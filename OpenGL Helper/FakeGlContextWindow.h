﻿#pragma once
#include "WWindow.h"

namespace glhelper {

	using namespace winhelper;

	class FakeGlContextWindow final : public WWindow
	{
	public:
		explicit FakeGlContextWindow();

		~FakeGlContextWindow() override;
		
	private:
		static WClass& GetFakeGlContextWindow();
	};
}
