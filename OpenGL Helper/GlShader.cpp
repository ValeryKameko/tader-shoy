#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>

#include "GlShader.h"
#include <vector>
#include "GlException.h"
#include "GlCompilationException.h"

namespace glhelper {

	GlShader::GlShader(
		const std::string& name,
		const std::string& source,
		GLenum shaderType)
		: Name(name), Source(source)
	{
		Id = glCreateShader(shaderType);

		const char* shaderSource = Source.c_str();
		glShaderSource(Id, 1, &shaderSource, nullptr);
	}

	GlShader::~GlShader()
	{
		glDeleteShader(Id);
	}

	bool GlShader::IsCompilationSuccessful()
	{
		GLint compilationStatus;
		glGetShaderiv(Id, GL_COMPILE_STATUS, &compilationStatus);
		return compilationStatus != GL_FALSE;
	}

	std::string GlShader::GetCompilationLog()
	{
		GLint maxLogLength = 0;
		glGetShaderiv(Id, GL_INFO_LOG_LENGTH, &maxLogLength);

		GLsizei logLength;
		std::vector<GLchar> infoLog(maxLogLength);

		glGetShaderInfoLog(Id, maxLogLength, &logLength, infoLog.data());

		std::string logString;
		for (GLchar ch : infoLog)
			logString += char(ch);

		return logString;
	}

	GLuint GlShader::GetId() const
	{
		return Id;
	}

	void GlShader::Compile()
	{
		glCompileShader(Id);
		if (!IsCompilationSuccessful()) {
			std::string compilationLog = GetCompilationLog();
			throw GlShaderCompilationException("Shader " + Name + " compile error:\n" + compilationLog);
		}
	}
}
