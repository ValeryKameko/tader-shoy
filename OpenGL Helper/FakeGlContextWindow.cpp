﻿#include "FakeGlContextWindow.h"
#include "WClass.h"

namespace glhelper {

	FakeGlContextWindow::FakeGlContextWindow()
	{
		InitializeWindow(
			nullptr,
			GetFakeGlContextWindow(),
			L"Fake_Gl_Context_Window",
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
			WS_EX_OVERLAPPEDWINDOW,
			0, 0,
			0, 0);
	}

	FakeGlContextWindow::~FakeGlContextWindow()
	{
	}

	WClass& FakeGlContextWindow::GetFakeGlContextWindow()
	{
		static WClass fakeGlContextWindowClass{
			L"Fake_Gl_Context_Window_Class",
			CS_OWNDC
		};
		return fakeGlContextWindowClass;
	}

	
}
