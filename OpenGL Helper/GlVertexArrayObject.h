#pragma once

#include <windows.h>
#include <GL\glew.h>
#include <GL\gl.h>
#include <string>


namespace glhelper {
	
	class GlVertexArrayObject final
	{
	public:
		explicit GlVertexArrayObject(std::string name);
		~GlVertexArrayObject();

		GLuint GetId();
		void Bind();

		void SetAttributePointer(
			GLuint attributeIdx, 
			GLint size, 
			GLenum type,
			GLint stride,
			GLboolean isNormalized = GL_FALSE,
			const GLvoid* offset = static_cast<void*>(0));
		void EnableAttributePointer(GLuint attributeIdx);
		void DisableAttributePointer(GLuint attributeIdx);
		
	private:
		GLuint Id;
		std::string Name;
	};
}

