﻿#pragma once

#include <memory>
#include <Windows.h>
#include <vector>

namespace glhelper {

	class FakeGlContextWindow;
	
	class FakeGlContext final
	{
	public:
		~FakeGlContext();
		
		static FakeGlContext& Instance();

		void MakeCurrent();
		void FreeCurrent();
		
	private:
		static std::unique_ptr<PIXELFORMATDESCRIPTOR> FindClosestPixelFormatDescriptor(
			HDC deviceContext,
			const PIXELFORMATDESCRIPTOR& pfd,
			int &index);
		static PIXELFORMATDESCRIPTOR GetRequiredPixelFormatDescriptor();
		void SetupPixelFormatDescriptor(HDC deviceContext);
		void SetupFakeRenderingContext(HDC deviceContext);
		
		FakeGlContext();
		
		HGLRC RenderContext;
		HDC DeviceContext;
		
		std::unique_ptr<FakeGlContextWindow> FakeWindow;
	};
}
