#include "GlException.h"

namespace glhelper {
	
	GlException::GlException(const char* message)
		: Message(message)
	{
	}

	GlException::GlException(const std::string& message)
		: Message(message)
	{
	}

	char const* GlException::what() const
	{
		return Message.c_str();
	}

}