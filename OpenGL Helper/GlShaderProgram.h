#pragma once

#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <memory>
#include <vector>
#include <string>

namespace glhelper {

	class GlShader;
	
	class GlShaderProgram final
	{
	public:
		GlShaderProgram(const std::string& name);
		~GlShaderProgram();

		GLuint GetId();
		
		void AddShader(const std::shared_ptr<GlShader>& shader);
		bool IsValid();
		void Compile();
		void Use();
		
	private:
		GLuint Id;
		
		std::vector<std::shared_ptr<GlShader>> Shaders;
		std::string Name;

		bool IsLinked();
		std::string GetProgramLog();
	};
}
