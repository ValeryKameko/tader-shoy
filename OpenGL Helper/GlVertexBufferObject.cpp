#include "GlVertexBufferObject.h"
#include "GlException.h"

namespace glhelper {
	
	GlVertexBufferObject::GlVertexBufferObject(std::string name)
		: Name(name), Data(nullptr)
	{
		glGenBuffers(1, &Id);
	}

	GlVertexBufferObject::~GlVertexBufferObject()
	{
		glDeleteBuffers(1, &Id);
		if (Data)
			delete[] Data;

	}

	GLuint GlVertexBufferObject::GetId()
	{
		return Id;
	}

	void GlVertexBufferObject::Bind(GLenum target)
	{
		glBindBuffer(target, Id);
	}

	void GlVertexBufferObject::SetData(GLenum target, const void* data, size_t size, GLenum usage)
	{
		Data = new char[size];
		Size = size;
		Write(0, data, size);
		glBufferData(target, size, Data, usage);
	}

	void GlVertexBufferObject::Write(size_t offset, const void* data, size_t size)
	{
		if (offset + size > Size)
			throw GlException("Write buffer out of range");
		memcpy(static_cast<char*>(Data) + offset, data, size);
	}

	void GlVertexBufferObject::Read(size_t offset, void* data, size_t size)
	{
		if (offset + size > Size)
			throw GlException("Read buffer out of range");
		memcpy(data, static_cast<char*>(Data) + offset, size);
	}
}
