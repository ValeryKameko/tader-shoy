#pragma once

#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <string>

namespace glhelper {
	
	class GlVertexBufferObject final
	{
	public:
		GlVertexBufferObject(std::string name);
		~GlVertexBufferObject();
		
		GLuint GetId();

		void Bind(GLenum target);
		void SetData(GLenum target, const void* data, size_t size, GLenum usage);

		void Write(size_t offset, const void* data, size_t size);
		void Read(size_t offset, void* data, size_t size);
		
	private:
		GLuint Id;
		std::string Name;
		
		void* Data;
		size_t Size;
	};
}

