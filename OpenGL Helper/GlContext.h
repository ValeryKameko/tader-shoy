﻿#pragma once

#include <Windows.h>
#include <memory>
#include <vector>
#include <GL/glew.h>
#include <GL/GL.h>
#include <map>

namespace glhelper
{
	class FakeGlContext;
	
	class GlContext final
	{
	public:
		GlContext(HDC DeviceContext, int majorVersion, int minorVersion);
		~GlContext();

		void MakeCurrent();
		void FreeCurrent();

	private:
		void SetupGlew();
		void SetupDebugHandler();
		void SetupPixelFormatDescriptor(HDC deviceContext);
		void SetupRenderContext(
			HDC deviceContext,
			const std::vector<int>& contextAttributes);

		std::vector<int> CreateRenderingContextAttributes(
			int majorVersion,
			int minorVersion);

		std::unique_ptr<PIXELFORMATDESCRIPTOR> FindClosestPixelFormatDescriptor(
			HDC deviceContext,
			const std::vector<int>& pfdAttributes,
			int& index);

		HGLRC RenderContext;
		HDC DeviceContext;
		
		static const std::vector<int> REQUIRED_PIXEL_FORMAT_ATTRIBUTES;
		static const std::map<int, LPCSTR> DEBUG_SOURCE_TO_STRING;
		static const std::map<int, LPCSTR> DEBUG_TYPE_TO_STRING;
		static const std::map<int, LPCSTR> DEBUG_SEVERITY_TO_STRING;
		
		static void APIENTRY DebugHandler(
			GLenum source,
			GLenum type,
			GLuint id,
			GLenum severity,
			GLsizei length,
			const GLchar * message,
			void* userParam);
	};

} // namespace glhelper
