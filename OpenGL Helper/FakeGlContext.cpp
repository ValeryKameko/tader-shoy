﻿#include "FakeGlContext.h"
#include "FakeGlContextWindow.h"
#include "GlException.h"

namespace glhelper {

	FakeGlContext::~FakeGlContext()
	{
		FakeWindow->ReleaseDeviceContext(DeviceContext);
		FreeCurrent();
		wglDeleteContext(RenderContext);
	}

	FakeGlContext& FakeGlContext::Instance()
	{
		static FakeGlContext fakeContext;
		return fakeContext;
	}

	std::unique_ptr<PIXELFORMATDESCRIPTOR> FakeGlContext::FindClosestPixelFormatDescriptor(
		HDC deviceContext,
		const PIXELFORMATDESCRIPTOR& pfd, 
		int& index)
	{
		auto closestPfd = std::make_unique<PIXELFORMATDESCRIPTOR>();

		index = ::ChoosePixelFormat(deviceContext, &pfd);
		if (index == 0)
			return nullptr;

		int result = DescribePixelFormat(
			deviceContext,
			index,
			sizeof(PIXELFORMATDESCRIPTOR),
			closestPfd.get());
		
		if (!result)
			return nullptr;
		
		return closestPfd;
	}

	PIXELFORMATDESCRIPTOR FakeGlContext::GetRequiredPixelFormatDescriptor()
	{
		PIXELFORMATDESCRIPTOR pfd{};
		pfd.nSize = sizeof(pfd);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 32;
		pfd.cRedBits = 8;
		pfd.cGreenBits = 8;
		pfd.cBlueBits = 8;
		pfd.cAlphaBits = 8;
		pfd.cDepthBits = 32;
		pfd.iLayerType = PFD_MAIN_PLANE;
		return pfd;
	}

	void FakeGlContext::SetupPixelFormatDescriptor(HDC deviceContext)
	{
		auto requiredPfd = GetRequiredPixelFormatDescriptor();

		int index;
		auto pfd = FindClosestPixelFormatDescriptor(deviceContext, requiredPfd, index);
		if (!pfd.get())
			throw GlException("Cannot find closest Pixel Format Descriptor");

		if (!SetPixelFormat(deviceContext, index, pfd.get()))
			throw GlException("Cannot setup Pixel Format Descriptor");
	}

	void FakeGlContext::SetupFakeRenderingContext(HDC deviceContext) 
	{
		RenderContext = wglCreateContext(deviceContext);
		if (!RenderContext)
			throw GlException("Cannot setup Rendering Context");
	}

	FakeGlContext::FakeGlContext()
		: RenderContext(nullptr),
		DeviceContext(nullptr),
		FakeWindow(new FakeGlContextWindow{})
	{
		DeviceContext = FakeWindow->GetDeviceContext();
		SetupPixelFormatDescriptor(DeviceContext);
		SetupFakeRenderingContext(DeviceContext);
		MakeCurrent();
	}

	void FakeGlContext::MakeCurrent() 
	{
		if (!wglMakeCurrent(DeviceContext, RenderContext))
			throw GlException("Cannot make current Rendering Context");
	}

	void FakeGlContext::FreeCurrent() 
	{
		if (!wglMakeCurrent(DeviceContext, nullptr))
			throw GlException("Cannot free current Rendering Context");
	}

}
