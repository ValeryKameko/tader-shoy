#include "GlVertexArrayObject.h"

namespace glhelper {
		
	GlVertexArrayObject::GlVertexArrayObject(std::string name)
		: Name(name)
	{
		glGenVertexArrays(1, &Id);
	}

	GlVertexArrayObject::~GlVertexArrayObject()
	{
		glDeleteVertexArrays(1, &Id);
	}

	GLuint GlVertexArrayObject::GetId()
	{
		return Id;
	}

	void GlVertexArrayObject::Bind()
	{
		glBindVertexArray(Id);
	}

	void GlVertexArrayObject::SetAttributePointer(
		GLuint attributeIdx, 
		GLint size, 
		GLenum type,
		GLint stride,
		GLboolean isNormalized, 
		const GLvoid* offset)
	{
		glVertexAttribPointer(attributeIdx, size, type, isNormalized, stride, offset);
	}

	void GlVertexArrayObject::EnableAttributePointer(GLuint attributeIdx)
	{
		glEnableVertexAttribArray(attributeIdx);
	}

	void GlVertexArrayObject::DisableAttributePointer(GLuint attributeIdx)
	{
		glDisableVertexAttribArray(attributeIdx);
	}
}
