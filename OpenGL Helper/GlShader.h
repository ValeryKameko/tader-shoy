#pragma once

#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <string>

namespace glhelper {

	class GlShader final
	{
	public:
		GlShader(
			const std::string& name,
			const std::string& source,
			GLenum shaderType);
		~GlShader();

		bool IsCompilationSuccessful();
		std::string GetCompilationLog();
		void Compile();
		GLuint GetId() const;

	private:
		GLuint Id;
		std::string Name;
		std::string Source;
	};
}

