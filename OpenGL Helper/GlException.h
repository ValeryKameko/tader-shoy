#pragma once
#include <exception>
#include <string>

namespace glhelper {
	
	class GlException : public std::exception
	{
	public:
		explicit GlException(const char* message);
		explicit GlException(const std::string& message);

		char const* what() const override;
	private:
		std::string Message;
	};
}
