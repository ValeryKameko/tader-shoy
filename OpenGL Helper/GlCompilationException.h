#pragma once

#include "GlException.h"

namespace glhelper {

	class GlCompilationException : public GlException
	{
	public:
		explicit GlCompilationException(const char* message);
		explicit GlCompilationException(const std::string& message);
	};

	class GlShaderCompilationException final : public GlCompilationException
	{
	public:
		explicit GlShaderCompilationException(const char* message);
		explicit GlShaderCompilationException(const std::string& message);
	};

	class GlProgramBundlingException final : public GlCompilationException
	{
	public:
		explicit GlProgramBundlingException(const char* message);
		explicit GlProgramBundlingException(const std::string& message);
	};
}