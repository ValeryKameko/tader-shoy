#pragma once

#include <WWindow.h>
#include <memory>
#include "ViewportWindow.h"
#include <WRichEditControl.h>
#include <WTabControl.h>
#include <WButtonControl.h>
#include "ShaderInputWindow.h"
#include "Shader Environment/ShaderEnvironment.h"
#include <WComboBoxControl.h>


namespace tadershoy {
	
	using namespace winhelper;
	using namespace wincontrol;
	
	class MainWindow final : public WWindow
	{
	public:
		explicit MainWindow();
		
		~MainWindow() override;
	protected:
		LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) override;

	private:
		void CompileProgram(const std::shared_ptr<Renderer>& renderer);
		HMENU CreateWindowMenu();

		LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnRunButtonClick(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnChangeEnvironment(UINT uMsg, WPARAM wParam, LPARAM lParam);

		void OnOpenShaderFile();
		void OnSaveShaderFile();

		std::unique_ptr<WComboBoxControl> EnvironmentComboBox;
		std::unique_ptr<WButtonControl> RunButton;
		std::unique_ptr<ViewportWindow> Viewport;
		std::vector<std::shared_ptr<ShaderInputWindow>> ShaderInputEdits;
		std::unique_ptr<WRichEditControl> CompilationLogOutputEdit;
		std::unique_ptr<WTabControl> TabControl;

		std::vector<std::shared_ptr<ShaderEnvironment>> ShaderEnvironments;
		std::shared_ptr<ShaderEnvironment> CurrentEnvironment;
	};
}
