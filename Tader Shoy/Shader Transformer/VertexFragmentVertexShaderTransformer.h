#pragma once

#include "ShaderTransformer.h"

namespace tadershoy {

	class VertexFragmentVertexShaderTransformer : public ShaderTransformer
	{
	public:
		VertexFragmentVertexShaderTransformer();

		virtual std::string ProcessShader(const std::string& source) const override;

		virtual std::string ProcessErrorString(const std::string& error) const override;
	};
}

