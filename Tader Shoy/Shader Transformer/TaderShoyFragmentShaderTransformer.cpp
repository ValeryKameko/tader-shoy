#include "TaderShoyFragmentShaderTransformer.h"
#include <sstream>
#include <regex>

namespace tadershoy {

	const static std::string SOURCE_PREFIX =
		R"(#version 440
uniform vec2      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click

out vec4 gl_FragColor;

void mainImage( out vec4 fragColor, in vec2 fragCoord );

void main()
{
	mainImage(gl_FragColor, gl_FragCoord.xy);
}

)";

	TaderShoyFragmentShaderTransformer::TaderShoyFragmentShaderTransformer()
	{
	}

	std::string TaderShoyFragmentShaderTransformer::ProcessShader(const std::string& source) const
	{
		return SOURCE_PREFIX + source;
	}

	std::string TaderShoyFragmentShaderTransformer::ProcessErrorString(const std::string& error) const
	{
		std::istringstream prefixStream{ SOURCE_PREFIX };
		prefixStream.unsetf(std::ios_base::skipws);

		int lineCount = std::count(
			std::istream_iterator<char>{prefixStream},
			std::istream_iterator<char>(),
			'\n');

		std::ostringstream outputError;
		std::regex regex(R"(\d+:(\d+))");
		std::sregex_iterator matchBegin(error.begin(), error.end(), regex), matchEnd;
		size_t lastPosition = 0;
		for (; matchBegin != matchEnd; matchBegin++) {
			std::string lineString{ matchBegin->operator[](1) };
			std::istringstream lineStream{ lineString };
			int lineNumber;
			lineStream >> lineNumber;
			std::string fullMatch = matchBegin->operator[](0).str();
			std::string otherString = matchBegin->operator[](0).str().substr(0, fullMatch.size() - lineString.size());
			lastPosition = (size_t)matchBegin->position() + matchBegin->length();

			lineNumber -= lineCount - 1;
			outputError << matchBegin->prefix() << otherString << lineNumber;
		}
		outputError << error.substr(lastPosition);

		return outputError.str();
	}

}
