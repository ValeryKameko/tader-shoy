#pragma once
#include <string>

namespace tadershoy {

	class ShaderTransformer
	{
	public:
		virtual std::string ProcessShader(const std::string& source) const = 0;
		virtual std::string ProcessErrorString(const std::string& error) const = 0;

	protected:
		ShaderTransformer();
	};
}

