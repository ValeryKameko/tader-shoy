#pragma once

#include <WWindow.h>
#include <string>
#include <WRichEditControl.h>
#include "ShaderInput.h"

namespace tadershoy {

	using namespace winhelper;
	using namespace wincontrol;

	class ShaderInputWindow final : public WRichEditControl
	{
	public:
		ShaderInputWindow(const ShaderInput& type, WWindow* parent = nullptr, HMENU menu = nullptr);

		ShaderInput GetType();

	private:
		ShaderInput Type;
		std::wstring WideSource;
	};
}

