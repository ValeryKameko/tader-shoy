﻿#include "ViewportWindow.h"
#include "WClass.h"

#include <Windows.h>
#include <gl\glew.h>
#include <gl\GL.h>
#include "GlException.h"

namespace tadershoy {

	constexpr int RENDER_CONTEXT_MAJOR_VERSION = 4;
	constexpr int RENDER_CONTEXT_MINOR_VERSION = 0;
	constexpr int IDT_RENDER_TIMER = 1;
	constexpr int FPS = 60;
	constexpr int DELAY = 1000 / FPS;

	ViewportWindow::ViewportWindow(WWindow* parent, HMENU menu)
	{
		InitializeWindow(
			parent,
			GetViewportWindowClass(),
			L"Tader Shoy Viewport",
			WS_CHILD | WS_VISIBLE,
			0,
			menu);
	}

	ViewportWindow::~ViewportWindow()
	{
	}

	std::shared_ptr<GlContext> ViewportWindow::GetRenderContext()
	{
		return RenderContext;
	}

	void ViewportWindow::SetRenderer(const std::shared_ptr<tadershoy::Renderer>& renderer)
	{
		Time = 0;

		if (Renderer)
			Renderer->Finalize();

		SetTimer(GetHwnd(),       
			IDT_RENDER_TIMER,
			DELAY,
			(TIMERPROC)NULL);

		Renderer = renderer;
		Redraw(NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
	}

	LRESULT ViewportWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_CREATE:
			return OnCreate(uMsg, wParam, lParam);
		case WM_DESTROY:
			return OnDestroy(uMsg, wParam, lParam);
		case WM_SIZE:
			return OnSize(uMsg, wParam, lParam);
		case WM_PAINT:
			return OnPaint(uMsg, wParam, lParam);
		case WM_ERASEBKGND:
			return OnEraseBackground(uMsg, wParam, lParam);
		case WM_TIMER:
			return OnTimer(uMsg, wParam, lParam);
		default:
			return WWindow::HandleMessage(uMsg, wParam, lParam);
		}
	}

	LRESULT ViewportWindow::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		KillTimer(GetHwnd(), IDT_RENDER_TIMER);

		if (Renderer)
			Renderer->Finalize();
		
		RenderContext.reset();
		if (DeviceContext) {
			ReleaseDeviceContext(DeviceContext);
			DeviceContext = nullptr;
		}

		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT ViewportWindow::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT ViewportWindow::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		ULONGLONG currentTime = GetTickCount64();
		TimeDelta = GetTickCount() - Time;
		Time += TimeDelta;

		Redraw(nullptr, nullptr, RDW_INTERNALPAINT | RDW_INVALIDATE);
		Update();
		return 0;
	}

	WClass ViewportWindow::GetViewportWindowClass()
	{
		static WClass viewportWindowClass{
			L"Viewport_Window_Class",
			CS_OWNDC
		};
		return viewportWindowClass;
	}

	LRESULT ViewportWindow::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		DeviceContext = GetDeviceContext();
		RenderContext = std::make_unique<GlContext>(
			DeviceContext,
			RENDER_CONTEXT_MAJOR_VERSION,
			RENDER_CONTEXT_MINOR_VERSION);
		
		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT ViewportWindow::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		RenderContext->MakeCurrent();

		if (Renderer) {
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);

			glClearColor(0.129f, 0.586f, 0.949f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			RECT rect = GetClientRect();
			Renderer->SetSize(rect.right - rect.left, rect.bottom - rect.top);
			Renderer->SetTime(Time / float(1000));
			Renderer->SetTimeDelta(TimeDelta / float(1000));

			Renderer->Render();
		}
		else {
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		glFlush();
		SwapBuffers(DeviceContext);
		RenderContext->FreeCurrent();
		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT ViewportWindow::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		int width = LOWORD(lParam);
		int height = HIWORD(lParam);
		if (height == 0)
			height = 1;

		RenderContext->MakeCurrent();

		glViewport(0, 0, width, height);
		
		RenderContext->FreeCurrent();

		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}
}
