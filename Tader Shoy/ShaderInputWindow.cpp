#include "ShaderInputWindow.h"
#include <regex>

namespace tadershoy {

	ShaderInputWindow::ShaderInputWindow(const ShaderInput& type, WWindow* parent, HMENU menu)
		: WRichEditControl{
			ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_VSCROLL | WS_HSCROLL | WS_BORDER,
			WS_EX_CLIENTEDGE,
			parent,
			menu },
			Type{ type }
	{

		std::string source{ type.GetDefaultSource() };

		std::regex regex("\n");
		source = std::regex_replace(source, regex, "\r\n");
		WideSource = std::wstring{ source.begin(), source.end() };
		SendMessageW(WM_SETTEXT, WPARAM(0), LPARAM(WideSource.c_str()));

	}

	ShaderInput ShaderInputWindow::GetType()
	{
		return Type;
	}
}
