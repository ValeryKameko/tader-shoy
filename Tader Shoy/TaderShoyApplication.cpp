#include "TaderShoyApplication.h"
#include "MainWindow.h"
#include <memory>

namespace tadershoy
{
	TaderShoyApplication::TaderShoyApplication(
		HINSTANCE hInstance, 
		HINSTANCE hPrevInstance, 
		LPCWSTR lpCmdLine,
		int nCmdShow)
		: WApplication(hInstance)
	{
		NCmdShow = nCmdShow;
	}

	INT TaderShoyApplication::Run()
	{
		auto mainWindow = std::make_unique<MainWindow>();

		mainWindow->Show(NCmdShow);
		
		return WApplication::Run();
	}
}
