﻿#include <Windows.h>
#include "TaderShoyApplication.h"
#include "WException.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


int WINAPI wWinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	PWSTR pCmdLine,
	int nCmdShow)
{
	tadershoy::TaderShoyApplication application{
		hInstance,
		hPrevInstance,
		pCmdLine,
		nCmdShow
	};
	try {
		return application.Run();
	}
	catch (const std::exception &e) {
		std::string message{ e.what() };
		std::wstring wideMessage{ message.cbegin(), message.cend() };
		MessageBox(nullptr, wideMessage.c_str(), L"Error", MB_OK | MB_ICONERROR);
		return -1;
	}
}
