#include "ShaderInput.h"

namespace tadershoy {

	ShaderInput::ShaderInput(
		GLenum shaderType,
		std::wstring name,
		std::string defaultSource,
		std::shared_ptr<ShaderTransformer> transformer)
		: ShaderType(shaderType),
		Name(name), Transformer(transformer), DefaultSource(defaultSource)
	{
	}

	ShaderTransformer& ShaderInput::GetShaderTransformer() const
	{
		return *Transformer;
	}

	GLenum ShaderInput::GetShaderType() const
	{
		return ShaderType;
	}

	std::wstring ShaderInput::GetName() const
	{
		return Name;
	}

	std::string ShaderInput::GetDefaultSource() const
	{
		return DefaultSource;
	}

}