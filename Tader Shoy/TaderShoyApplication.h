#pragma once
#include <WApplication.h>

namespace tadershoy
{

	using namespace winhelper;

	class TaderShoyApplication final : public WApplication
	{
	public:
		explicit TaderShoyApplication(
			HINSTANCE hInstance, 
			HINSTANCE hPrevInstance, 
			LPCWSTR lpCmdLine, 
			int nCmdShow);

		INT Run() override;
	private:
		int NCmdShow;
	};
}

