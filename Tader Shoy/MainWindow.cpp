#include "MainWindow.h"
#include "WClass.h"
#include "WApplication.h"
#include <iostream>
#include <CommCtrl.h>
#include <GlShader.h>
#include <GlCompilationException.h>
#include <regex>
#include <fstream>
#include "ShaderInput.h"
#include "Shader Transformer/ShaderTransformer.h"
#include "Shader Environment/VertexFragmentShaderEnvironment.h"
#include "Shader Environment/TaderShoyShaderEnvironment.h"

namespace tadershoy {

	constexpr int VIEWPORT_MARGIN = 10;
	constexpr int INPUT_EDIT_MARGIN = 10;
	constexpr int COMPILATION_LOG_OUTPUT_EDIT_MARGIN = 10;
	constexpr int COMPILATION_LOG_OUTPUT_EDIT_HEIGHT = 200;
	constexpr int ENVIRONMENT_COMBOBOX_WIDTH = 170;

	constexpr int BUTTON_WIDTH = 70;
	constexpr int BUTTONS_AREA_MARGIN = 10;
	constexpr int BUTTONS_AREA_HEIGHT = 50;

	const HMENU VIEWPORT_MENU = HMENU(1);
	const HMENU COMPILATION_LOG_OUTPUT_EDIT_MENU = HMENU(2);
	const HMENU RUN_BUTTON_MENU = HMENU(3);
	const HMENU ENVIRONMENT_COMBOBOX_MENU = HMENU(4);

	const int IDM_OPEN_FILE = 1;
	const int IDM_SAVE_FILE = 2;
	const int IDM_EXIT = 3;
	const int FILE_PATH_BUFFER_SIZE = MAX_PATH + 512;


	MainWindow::MainWindow()
	{
		InitializeWindow(
			nullptr,
			WClass::GetDefaultWindowClass(),
			L"Main window",
			WS_OVERLAPPEDWINDOW | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
			WS_EX_OVERLAPPEDWINDOW);

		HMENU menu = CreateWindowMenu();
		SetMenu(GetHwnd(), menu);
	}

	MainWindow::~MainWindow()
	{

	}

	LRESULT MainWindow::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		ShaderEnvironments = {
			std::make_shared<VertexFragmentShaderEnvironment>(),
			std::make_shared<TaderShoyShaderEnvironment>()
		};

		TabControl = std::make_unique<WTabControl>(
			WS_BORDER | WS_VISIBLE,
			0,
			this);

		EnvironmentComboBox = std::make_unique<WComboBoxControl>(
			CBS_DROPDOWNLIST | WS_VISIBLE,
			0,
			this,
			ENVIRONMENT_COMBOBOX_MENU);

		for (const std::shared_ptr<ShaderEnvironment>& environment : ShaderEnvironments) {
			std::string name = environment->GetName();
			std::wstring wideName{ name.cbegin(), name.cend() };
			EnvironmentComboBox->Add(wideName.c_str());
		}

		EnvironmentComboBox->Select(0);
		OnChangeEnvironment(0, 0, 0);

		Viewport = std::make_unique<ViewportWindow>(this, VIEWPORT_MENU);
		Viewport->Show(SW_SHOW);

		CompilationLogOutputEdit = std::make_unique<WRichEditControl>(
			ES_READONLY | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_VSCROLL | WS_HSCROLL | WS_BORDER | WS_VISIBLE,
			WS_EX_CLIENTEDGE,
			this,
			COMPILATION_LOG_OUTPUT_EDIT_MENU);

		RunButton = std::make_unique<WButtonControl>(
			L"Run",
			BS_DEFPUSHBUTTON | WS_VISIBLE,
			0,
			this,
			RUN_BUTTON_MENU);

		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT MainWindow::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		WApplication::Quit();
		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT MainWindow::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		PAINTSTRUCT paintStruct;
		HDC deviceContext = BeginPaint(GetHwnd(), &paintStruct);

		RECT clientRect = GetClientRect();
		HBRUSH brush = CreateSolidBrush(RGB(255, 255, 255));

		FillRect(deviceContext, &clientRect, brush);
		EndPaint(GetHwnd(), &paintStruct);
		return WWindow::HandleMessage(uMsg, wParam, lParam);
	}

	LRESULT MainWindow::OnRunButtonClick(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		auto renderContext = Viewport->GetRenderContext();

		try {
			renderContext->MakeCurrent();

			auto renderer = CurrentEnvironment->GetRenderer();
			renderer->Setup();
			CompileProgram(renderer);
			Viewport->SetRenderer(renderer);
			renderContext->FreeCurrent();
		}
		catch (const glhelper::GlCompilationException & e) {
			std::string log{ e.what() };

			std::regex regex("\n");
			log = std::regex_replace(log, regex, "\r\n");
			std::wstring wideLog{ log.begin(), log.end() };

			CompilationLogOutputEdit->SendMessageW(WM_SETTEXT, WPARAM(0), LPARAM(wideLog.c_str()));

			MessageBoxW(nullptr, L"Compilation error", L"Error", MB_OK);
		}
		return 0;
	}

	LRESULT MainWindow::OnChangeEnvironment(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		int index = EnvironmentComboBox->GetSelection();

		CurrentEnvironment = ShaderEnvironments[index];

		ShaderInputEdits.clear();
		TabControl->Clear();

		for (const ShaderInput& type : CurrentEnvironment->GetTypes()) {
			ShaderInputEdits.emplace_back(new ShaderInputWindow{
				type,
				TabControl.get() });
			TabControl->AddTab(type.GetName().c_str(), ShaderInputEdits.back());
		}

		return 0;
	}

	LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_CREATE:
			return OnCreate(uMsg, wParam, lParam);
		case WM_SIZE:
			return OnSize(uMsg, wParam, lParam);
		case WM_DESTROY:
			return OnDestroy(uMsg, wParam, lParam);
		case WM_PAINT:
			return OnPaint(uMsg, wParam, lParam);
		case WM_COMMAND:
		{
			if (HMENU(LOWORD(wParam)) == RunButton->GetMenu())
				return OnRunButtonClick(uMsg, wParam, lParam);
			if (HMENU(LOWORD(wParam)) == EnvironmentComboBox->GetMenu() &&
				HIWORD(wParam) == CBN_SELCHANGE)
				return OnChangeEnvironment(uMsg, wParam, lParam);
			switch (LOWORD(wParam)) {
			case IDM_EXIT:
				SendMessageW(WM_CLOSE, 0, 0);
				return 0;
			case IDM_OPEN_FILE:
				OnOpenShaderFile();
				return 0;
			case IDM_SAVE_FILE:
				OnSaveShaderFile();
				return 0;
			}
		}
		default:
			return WWindow::HandleMessage(uMsg, wParam, lParam);
		}
	}

	LRESULT MainWindow::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		int width = LOWORD(lParam);
		int height = HIWORD(lParam);

		Viewport->Reposition(
			VIEWPORT_MARGIN, VIEWPORT_MARGIN,
			width / 2 - 2 * VIEWPORT_MARGIN, height - 2 * VIEWPORT_MARGIN);
		TabControl->Reposition(
			INPUT_EDIT_MARGIN + width / 2, INPUT_EDIT_MARGIN + BUTTONS_AREA_HEIGHT,
			width / 2 - 2 * INPUT_EDIT_MARGIN, height - COMPILATION_LOG_OUTPUT_EDIT_HEIGHT - 2 * INPUT_EDIT_MARGIN - BUTTONS_AREA_HEIGHT);

		CompilationLogOutputEdit->Reposition(
			COMPILATION_LOG_OUTPUT_EDIT_MARGIN + width / 2,
			height - COMPILATION_LOG_OUTPUT_EDIT_HEIGHT,
			width / 2 - 2 * COMPILATION_LOG_OUTPUT_EDIT_MARGIN,
			COMPILATION_LOG_OUTPUT_EDIT_HEIGHT - 2 * COMPILATION_LOG_OUTPUT_EDIT_MARGIN);
		RunButton->Reposition(
			BUTTONS_AREA_MARGIN + width / 2, BUTTONS_AREA_MARGIN,
			BUTTON_WIDTH, BUTTONS_AREA_HEIGHT - 2 * BUTTONS_AREA_MARGIN);

		EnvironmentComboBox->Reposition(
			width - ENVIRONMENT_COMBOBOX_WIDTH, BUTTONS_AREA_MARGIN,
			ENVIRONMENT_COMBOBOX_WIDTH - 2 * BUTTONS_AREA_MARGIN, BUTTONS_AREA_HEIGHT - 2 * BUTTONS_AREA_MARGIN);
		return 0;
	}

	HMENU MainWindow::CreateWindowMenu()
	{
		HMENU windowMenu = CreateMenu();
		HMENU fileMenu = CreateMenu();

		AppendMenuW(fileMenu, MF_STRING, IDM_OPEN_FILE, L"&Open shader");
		AppendMenuW(fileMenu, MF_STRING, IDM_SAVE_FILE, L"&Save shader");
		AppendMenuW(fileMenu, MF_SEPARATOR, 0, NULL);
		AppendMenuW(fileMenu, MF_STRING, IDM_EXIT, L"&Exit");

		AppendMenuW(windowMenu, MF_POPUP, UINT_PTR(fileMenu), L"&File");
		return windowMenu;
	}


	void MainWindow::CompileProgram(const std::shared_ptr<Renderer>& renderer) {
		std::vector<std::shared_ptr<GlShader>> shaders;

		for (const auto& shaderInputEdit : ShaderInputEdits) {
			std::string source = shaderInputEdit->GetTextA();
			const ShaderTransformer& transformer = shaderInputEdit->GetType().GetShaderTransformer();

			source = transformer.ProcessShader(source);

			auto wideName = shaderInputEdit->GetType().GetName();
			std::string name{ wideName.cbegin(), wideName.cend() };

			auto shader = std::make_shared<GlShader>(
				name,
				source,
				shaderInputEdit->GetType().GetShaderType());

			try {
				shader->Compile();
			}
			catch (const GlShaderCompilationException & e) {
				std::string transformedWhat = transformer.ProcessErrorString(e.what());
				throw GlShaderCompilationException(transformedWhat);
			}
			shaders.push_back(shader);
		}

		renderer->CompileProgram(shaders);
	}

	void MainWindow::OnOpenShaderFile() {
		LPCWSTR filePathBuffer = (LPCWSTR)GlobalAlloc(GPTR, FILE_PATH_BUFFER_SIZE);

		OPENFILENAME of = { };
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = GetHwnd();
		of.hInstance = nullptr;
		of.lpstrFilter =
			L"GLSL shader files(*.frag, *.vert)\0*.frag;*.vert\0"\
			L"Text files(*.txt)\0*.txt\0"\
			L"All files(*.txt)\0*.*\0"\
			L"\0";
		of.lpstrCustomFilter = nullptr;
		of.nMaxCustFilter = 0;
		of.nFilterIndex = 1;
		of.lpstrFile = (LPWSTR)filePathBuffer;
		of.nMaxFile = FILE_PATH_BUFFER_SIZE;
		of.lpstrFileTitle = nullptr;
		of.nMaxFileTitle = 0;
		of.lpstrInitialDir = nullptr;
		of.lpstrTitle = L"Choose shaders";
		of.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST;
		of.nFileOffset = 0;
		of.nFileExtension = 0;
		of.lpstrDefExt = L"frag";
		of.lCustData = 0;
		of.lpfnHook = nullptr;
		of.lpTemplateName = nullptr;
		of.FlagsEx = 0;

		if (GetOpenFileName(&of)) {
			std::wifstream inFile{ filePathBuffer };
			std::wstring content{
				std::istreambuf_iterator<wchar_t>(inFile),
				std::istreambuf_iterator<wchar_t>() };

			int index = TabControl->GetSelection();
			ShaderInputEdits[index]->SendMessageW(WM_SETTEXT, WPARAM(0), LPARAM(content.c_str()));
		}
		GlobalFree(HGLOBAL(filePathBuffer));
	}


	void MainWindow::OnSaveShaderFile() {
		LPCWSTR filePathBuffer = (LPCWSTR)GlobalAlloc(GPTR, FILE_PATH_BUFFER_SIZE);

		OPENFILENAME of = { };
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = GetHwnd();
		of.hInstance = nullptr;
		of.lpstrFilter =
			L"GLSL shader files(*.frag, *.vert)\0*.frag;*.vert\0"\
			L"Text files(*.txt)\0*.txt\0"\
			L"All files(*.txt)\0*.*\0"\
			L"\0";
		of.lpstrCustomFilter = nullptr;
		of.nMaxCustFilter = 0;
		of.nFilterIndex = 1;
		of.lpstrFile = (LPWSTR)filePathBuffer;
		of.nMaxFile = FILE_PATH_BUFFER_SIZE;
		of.lpstrFileTitle = nullptr;
		of.nMaxFileTitle = 0;
		of.lpstrInitialDir = nullptr;
		of.lpstrTitle = L"Save shaders";
		of.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_OVERWRITEPROMPT;
		of.nFileOffset = 0;
		of.nFileExtension = 0;
		of.lpstrDefExt = L"frag";
		of.lCustData = 0;
		of.lpfnHook = nullptr;
		of.lpTemplateName = nullptr;
		of.FlagsEx = 0;

		if (GetSaveFileName(&of)) {
			int index = TabControl->GetSelection();
			std::string source = ShaderInputEdits[index]->GetTextA();

			std::ofstream outFile{ filePathBuffer };
			outFile << source;
			outFile.close();
		}
		GlobalFree(HGLOBAL(filePathBuffer));
	}
}
