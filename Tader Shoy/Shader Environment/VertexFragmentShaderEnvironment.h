#pragma once

#include "../ShaderInput.h"
#include "ShaderEnvironment.h"

namespace tadershoy {

	class VertexFragmentShaderEnvironment final : public ShaderEnvironment
	{
	public:
		virtual const std::vector<ShaderInput>& GetTypes() const override;

		virtual std::shared_ptr<Renderer> GetRenderer() const override;

		virtual const std::string& GetName() const override;
	};
}

