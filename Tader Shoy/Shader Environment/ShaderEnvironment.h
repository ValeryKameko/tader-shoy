#pragma once

#include <vector>
#include "../ShaderInput.h"
#include "../Renderer/Renderer.h"

namespace tadershoy {

	class ShaderEnvironment
	{
	public:
		virtual const std::vector<ShaderInput>& GetTypes() const = 0;

		virtual std::shared_ptr<Renderer> GetRenderer() const = 0;

		virtual const std::string& GetName() const = 0;

	protected:
		ShaderEnvironment();
	};
}

