#include "TaderShoyShaderEnvironment.h"
#include "../Shader Transformer/TaderShoyFragmentShaderTransformer.h"
#include "../Renderer/TaderShoyRenderer.h"

namespace tadershoy {

	const static std::string DEFAULT_FRAGMENT_SHADER_SOURCE =
		R"(void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*cos(iTime+uv.xyx+vec3(0,2,4));

    // Output to screen
    fragColor = vec4(col,1.0);
})";


	const static std::string NAME =
		"TaderShoy";

	const static std::vector<ShaderInput> TaderShoyTypes = {
		ShaderInput{
			GL_FRAGMENT_SHADER,
			L"Fragment shader",
			DEFAULT_FRAGMENT_SHADER_SOURCE,
			std::make_shared<TaderShoyFragmentShaderTransformer>()}
	};

	const std::vector<ShaderInput>& TaderShoyShaderEnvironment::GetTypes() const
	{
		return TaderShoyTypes;
	}

	std::shared_ptr<Renderer> TaderShoyShaderEnvironment::GetRenderer() const
	{
		return std::static_pointer_cast<Renderer>(std::make_shared<TaderShoyRenderer>());
	}

	const std::string& TaderShoyShaderEnvironment::GetName() const
	{
		return NAME;
	}
}