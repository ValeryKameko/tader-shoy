#include "VertexFragmentShaderEnvironment.h"
#include "../Shader Transformer/VertexFragmentVertexShaderTransformer.h"
#include "../Shader Transformer/VertexFragmentFragmentShaderTransformer.h"
#include "../Renderer/MeshRenderer.h"

namespace tadershoy {

	const static std::string DEFAULT_VERTEX_SHADER_SOURCE =
		R"(#version 440
in vec3 position;

void main()
{
	gl_Position = vec4(position, 1.0f);
})";

	const static std::string DEFAULT_FRAGMENT_SHADER_SOURCE =
		R"(#version 440
out vec4 color;

void main()
{
	color = vec4(1.0f, 0.5f, 0.2f, 1.0f);
})";

	const static std::string NAME =
		"Vertex + Fragment";

	const static std::vector<ShaderInput> VertexFragmentTypes = {
		ShaderInput{
			GL_VERTEX_SHADER,
			L"Vertex shader",
			DEFAULT_VERTEX_SHADER_SOURCE,
			std::make_shared<VertexFragmentVertexShaderTransformer>()},
		ShaderInput{
			GL_FRAGMENT_SHADER,
			L"Fragment shader",
			DEFAULT_FRAGMENT_SHADER_SOURCE,
			std::make_shared<VertexFragmentFragmentShaderTransformer>()}
	};

	const std::vector<ShaderInput>& VertexFragmentShaderEnvironment::GetTypes() const
	{
		return VertexFragmentTypes;
	}

	std::shared_ptr<Renderer> VertexFragmentShaderEnvironment::GetRenderer() const
	{
		return std::static_pointer_cast<Renderer>(std::make_shared<MeshRenderer>());
	}

	const std::string& VertexFragmentShaderEnvironment::GetName() const
	{
		return NAME;
	}
}