#include "TaderShoyRenderer.h"
#include "GlShader.h"

#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <memory>
#include <memory>
#include "GlVertexBufferObject.h"
#include "GlException.h"

namespace tadershoy {

	const static std::string VERTEX_SHADER_SOURCE =
		R"(#version 440
in vec3 position;

void main()
{
	gl_Position = vec4(position, 1.0f);
})";

	void TaderShoyRenderer::Setup()
	{
		float vertices[] = {
			 -1.0f, -1.0f, 0.0f,
			 -1.0f, +1.0f, 0.0f,
			 +1.0f, -1.0f, 0.0f,
			 +1.0f, +1.0f, 0.0f,
		};

		MeshVAO = std::make_shared<GlVertexArrayObject>("Rectangle VAO");
		MeshVAO->Bind();

		MeshDataVBO = std::make_shared<GlVertexBufferObject>("Rectangle VBO");
		MeshDataVBO->Bind(GL_ARRAY_BUFFER);
		MeshDataVBO->SetData(GL_ARRAY_BUFFER, vertices, sizeof(vertices), GL_STATIC_DRAW);
	}

	void TaderShoyRenderer::Render()
	{
		if (Program) {
			Program->Use();

			glUniform2f(iResolutionLocation, Width, Height);
			glUniform1f(iTimeLocation, Time);
			glUniform1f(iTimeDeltaLocation, TimeDelta);

			MeshVAO->Bind();
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

	void TaderShoyRenderer::Finalize()
	{
		MeshDataVBO.reset();
		MeshVAO.reset();
		Program.reset();
	}

	const GlShaderProgram& TaderShoyRenderer::GetProgram()
	{
		return *Program;
	}


	void TaderShoyRenderer::CompileProgram(const std::vector<std::shared_ptr<GlShader>>& shaders)
	{
		std::shared_ptr<GlShader> vertexShader = std::make_shared<GlShader>(
			"TaderShoy program vertex shader",
			VERTEX_SHADER_SOURCE,
			GL_VERTEX_SHADER);

		Program = std::make_shared<GlShaderProgram>("TaderShoy renderer program");
		for (const std::shared_ptr<GlShader>& shader : shaders) {
			Program->AddShader(shader);
		}
		vertexShader->Compile();
		Program->AddShader(vertexShader);
		Program->Compile();

		Program->Use();
		MeshVAO->Bind();
		MeshDataVBO->Bind(GL_ARRAY_BUFFER);

		int positionLocation = glGetAttribLocation(Program->GetId(), "position");

		MeshVAO->EnableAttributePointer(positionLocation);
		MeshVAO->SetAttributePointer(positionLocation, 3, GL_FLOAT, 3 * sizeof(float));

		iResolutionLocation = glGetUniformLocation(Program->GetId(), "iResolution");
		iTimeLocation = glGetUniformLocation(Program->GetId(), "iTime");
		iTimeDeltaLocation = glGetUniformLocation(Program->GetId(), "iTimeDelta");
	}
}
