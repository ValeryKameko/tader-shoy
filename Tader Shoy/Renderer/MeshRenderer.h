#pragma once
#include "GlShaderProgram.h"
#include "GlVertexArrayObject.h"
#include "GlVertexBufferObject.h"
#include "Renderer.h"

namespace tadershoy {

	using namespace glhelper;

	class MeshRenderer final : public Renderer
	{
	public:

		void Setup() override;
		void Render() override;
		void Finalize() override;

		const GlShaderProgram& GetProgram() override;
		void CompileProgram(const std::vector<std::shared_ptr<GlShader>>& shaders) override;

	private:
		std::shared_ptr<GlShaderProgram> Program;
		std::shared_ptr<GlVertexArrayObject> MeshVAO;
		std::shared_ptr<GlVertexBufferObject> MeshDataVBO;
	};
}

