#include "MeshRenderer.h"
#include "GlShader.h"

#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <memory>
#include <memory>
#include "GlVertexBufferObject.h"
#include "GlException.h"

namespace tadershoy {

	void MeshRenderer::Setup()
	{
		float vertices[] = {
			 0.0f,  0.5f, 0.0f,
			 0.5f, -0.5f, 0.0f,
			-0.5f, -0.5f, 0.0f,
		};

		MeshVAO = std::make_shared<GlVertexArrayObject>("Triangle VAO");
		MeshVAO->Bind();

		MeshDataVBO = std::make_shared<GlVertexBufferObject>("Triangle VBO");
		MeshDataVBO->Bind(GL_ARRAY_BUFFER);
		MeshDataVBO->SetData(GL_ARRAY_BUFFER, vertices, sizeof(vertices), GL_STATIC_DRAW);
	}

	void MeshRenderer::Render()
	{
		if (Program) {
			Program->Use();
			MeshVAO->Bind();
			glDrawArrays(GL_TRIANGLES, 0, 3);
		}
	}

	void MeshRenderer::Finalize()
	{
		MeshDataVBO.reset();
		MeshVAO.reset();
		Program.reset();
	}

	const GlShaderProgram& MeshRenderer::GetProgram()
	{
		return *Program;
	}

	void MeshRenderer::CompileProgram(const std::vector<std::shared_ptr<GlShader>>& shaders)
	{
		Program = std::make_shared<GlShaderProgram>("Mesh renderer program");
		for (const std::shared_ptr<GlShader>& shader : shaders) {
			shader->Compile();
			Program->AddShader(shader);
		}
		Program->Compile();

		Program->Use();
		MeshVAO->Bind();
		MeshDataVBO->Bind(GL_ARRAY_BUFFER);

		int positionLocation = glGetAttribLocation(Program->GetId(), "position");

		MeshVAO->EnableAttributePointer(positionLocation);
		MeshVAO->SetAttributePointer(positionLocation, 3, GL_FLOAT, 3 * sizeof(float));
	}
}
