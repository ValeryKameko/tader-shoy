#pragma once
#include <GlShaderProgram.h>
#include <GlVertexArrayObject.h>
#include <GlVertexBufferObject.h>
#include <GlShader.h>

namespace tadershoy {

	using namespace glhelper;

	class Renderer
	{
	public:

		virtual void Setup() = 0;
		virtual void Render() = 0;
		virtual void Finalize() = 0;

		void SetSize(float width, float height);
		void SetTime(float time);
		void SetTimeDelta(float timeDelta);
		void SetMousePosition(float x, float y);
		void SetMousePress(bool isPressed);

		virtual const GlShaderProgram& GetProgram() = 0;
		virtual void CompileProgram(const std::vector<std::shared_ptr<GlShader>>& shaders) = 0;

	protected:
		float Width;
		float Height;
		float Time;
		float TimeDelta;
		float MouseX;
		float MouseY;
		bool IsMousePressed;
	};
}

