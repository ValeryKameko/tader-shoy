#include "Renderer.h"

namespace tadershoy {

	void Renderer::SetSize(float width, float height) 
	{
		Width = width;
		Height = height;
	}

	void Renderer::SetTime(float time)
	{
		Time = time;
	}

	void Renderer::SetTimeDelta(float timeDelta)
	{
		TimeDelta = timeDelta;
	}

	void Renderer::SetMousePosition(float x, float y)
	{
		MouseX = x;
		MouseY = y;
	}

	void Renderer::SetMousePress(bool isPressed)
	{
		IsMousePressed = isPressed;
	}
}