﻿#pragma once

#include "WWindow.h"
#include "GlContext.h"
#include <memory>
#include "Renderer/Renderer.h"

namespace tadershoy {
	
	using namespace winhelper;
	using namespace glhelper;
	
	class ViewportWindow final : public WWindow
	{
	public:
		ViewportWindow(WWindow* parent, HMENU menu);
		~ViewportWindow();

		std::shared_ptr<GlContext> GetRenderContext();
		void SetRenderer(const std::shared_ptr<tadershoy::Renderer>& renderer);

	protected:
		LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) override;
		
	private:
		static WClass GetViewportWindowClass();
		LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam);
		LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam);
		
		HDC DeviceContext;
		std::shared_ptr<GlContext> RenderContext;
		std::shared_ptr<tadershoy::Renderer> Renderer;
		ULONGLONG TimeDelta;
		ULONGLONG Time;
	};

}
