#pragma once

#include <gl/glew.h>
#include <gl/gl.h>
#include <string>
#include <memory>
#include "Shader Transformer/ShaderTransformer.h"

namespace tadershoy {

	class ShaderInput final
	{
	public:
		ShaderInput(GLenum shaderType, std::wstring name, std::string defaultSource, std::shared_ptr<ShaderTransformer> transformer);

		ShaderTransformer& GetShaderTransformer() const;
		GLenum GetShaderType() const;
		std::wstring GetName() const;
		std::string GetDefaultSource() const;

	private:
		std::string DefaultSource;
		GLenum ShaderType;
		std::wstring Name;
		std::shared_ptr<ShaderTransformer> Transformer;
	};
}

